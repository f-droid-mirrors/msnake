/*
 * Copyright (C) 2007 The Android Open Source Project
 * Copyright (C) 2011 Mariano Alvarez Fernandez
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fgrim.msnake;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * MSnake: a simple game that everyone can enjoy.
 * 
 * This is an implementation of the classic Game "Snake", in which you control a
 * serpent roaming around the garden looking for apples. Be careful, though,
 * because when you catch one, not only will you become longer, but you'll move
 * faster. Running into yourself or the walls will end the game.
 * 
 * Derived from the Snake game in the Android SDK (M.Alvarez):
 * 
 * Added touch control
 * Added view score and record
 * Added menu walls on/off
 * Added menu big,normal,small size
 * Added menu about
 * Added save preferences
 * Added vibration
 * Added red pepper, slow down the snake
 * Added records dialog
 * Added alternate input turn mode
 * Added fast speed
 * and more
 */
public class MSnake extends Activity {

//    private static final String TAG = "MSnake";

    private SnakeView mSnakeView;
    
    private static String ICICLE_KEY = "snake-view";
    
    static final int DIALOG_ABOUT_ID = 0;
    static final int DIALOG_RECORDS_ID = 1;
    static final int DIALOG_WALLS_ID = 2;
    static final int DIALOG_SIZE_ID = 3;
    static final int DIALOG_SPEED_ID = 4;
    static final int DIALOG_NEWS_ID = 5;
    
    boolean dResetCancel;
    
    /**
     * Called when Activity is first created. Turns off the title bar, sets up
     * the content views, and fires up the SnakeView.
     * 
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       	setContentView(R.layout.snake_layout);

        mSnakeView = (SnakeView) findViewById(R.id.snake);
        mSnakeView.setTextView((TextView) findViewById(R.id.text));
        mSnakeView.setScoreView((TextView) findViewById(R.id.textscore));
        mSnakeView.setRecordView((TextView) findViewById(R.id.textrecord));

        int dHeight = getWindowManager().getDefaultDisplay().getHeight();
        int dWidth = getWindowManager().getDefaultDisplay().getWidth();
        if (dHeight < 320 || dWidth < 320) mSnakeView.noSmallSize = true;

        // Restore preferences
        SharedPreferences settings = getPreferences(0);
        mSnakeView.restorePreferences(settings);
        setCorrectButtons();

        if (savedInstanceState == null) {
            // We were just launched -- set up a new game
            mSnakeView.setMode(SnakeView.READY);
        } else {
            // We are being restored
            Bundle map = savedInstanceState.getBundle(ICICLE_KEY);
            if (map != null) {
                mSnakeView.restoreState(map);
            } else {
                mSnakeView.setMode(SnakeView.PAUSE);
            }
        }
        if (mSnakeView.showNews) showDialog(DIALOG_NEWS_ID);
//    	Log.d(TAG, "onCreate end");
    }

    protected void setCorrectButtons() {
        Button mButton[] = new Button[6];
      
        mButton[0] = (Button) findViewById(R.id.button0);
        mButton[1] = (Button) findViewById(R.id.button1);
        mButton[2] = (Button) findViewById(R.id.button2);
        mButton[3] = (Button) findViewById(R.id.button3);
        mButton[4] = (Button) findViewById(R.id.button4);
        mButton[5] = (Button) findViewById(R.id.button5);
        
        if (mSnakeView.tlrInputMode) {
        	mButton[0].setVisibility(View.VISIBLE);
        	mButton[1].setVisibility(View.GONE);
        	mButton[2].setVisibility(View.GONE);
        	mButton[3].setVisibility(View.GONE);
        	mButton[4].setVisibility(View.GONE);
        	mButton[5].setVisibility(View.VISIBLE);
        } else {
        	mButton[0].setVisibility(View.GONE);
        	mButton[1].setVisibility(View.VISIBLE);
        	mButton[2].setVisibility(View.VISIBLE);
        	mButton[3].setVisibility(View.VISIBLE);
        	mButton[4].setVisibility(View.VISIBLE);
        	mButton[5].setVisibility(View.GONE);
        }
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        Vibrator mvibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        mSnakeView.setVibrator(mvibrator);
//    	Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Pause the game along with the activity if RUNNING!!
        if (mSnakeView.getMode() == SnakeView.RUNNING) {
            mSnakeView.setMode(SnakeView.PAUSE);
        }
//    	Log.d(TAG, "onPause");
   }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences settings = getPreferences(0);
        mSnakeView.savePreferences(settings);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //Store the game state
        outState.putBundle(ICICLE_KEY, mSnakeView.saveState());
    }
    
    public void bIzquierda(View view) {
    	mSnakeView.bIzquierda();
    }
    
    public void bArriba(View view) {
    	mSnakeView.bArriba();
    }
    
    public void bAbajo(View view) {
    	mSnakeView.bAbajo();
    }
    
    public void bDerecha(View view) {
    	mSnakeView.bDerecha();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.snake_menu, menu);
    	menu.findItem(R.id.menu_about).setIcon(
        	getResources().getDrawable(android.R.drawable.ic_menu_info_details));
    	menu.findItem(R.id.menu_records).setIcon(
            getResources().getDrawable(android.R.drawable.ic_menu_view));
    	menu.findItem(R.id.menu_zoom).setIcon(
            getResources().getDrawable(android.R.drawable.ic_menu_zoom));
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	if (mSnakeView.getMode() == SnakeView.RUNNING)
            mSnakeView.setMode(SnakeView.PAUSE);
        menu.findItem(R.id.menu_walls).setIcon(getResources().getDrawable(
        	mSnakeView.mUseWalls ? android.R.drawable.button_onoff_indicator_on :
        	android.R.drawable.button_onoff_indicator_off));
        menu.findItem(R.id.menu_speed).setIcon(getResources().getDrawable(
            	mSnakeView.mFast ? android.R.drawable.button_onoff_indicator_on :
            	android.R.drawable.button_onoff_indicator_off));
        menu.findItem(R.id.menu_tlrinput).setIcon(getResources().getDrawable(
            mSnakeView.tlrInputMode ? android.R.drawable.button_onoff_indicator_on :
            android.R.drawable.button_onoff_indicator_off));
   	return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.menu_about:
        	showDialog(DIALOG_ABOUT_ID);
            return true;
        case R.id.menu_records:
        	showDialog(DIALOG_RECORDS_ID);
            return true;
        case R.id.menu_walls:
        	if (mSnakeView.getMode() == SnakeView.PAUSE)
        	    showDialog(DIALOG_WALLS_ID);
        	else {
          	    mSnakeView.changeWalls();
        	    item.setIcon(getResources().getDrawable(
               	    mSnakeView.mUseWalls ? android.R.drawable.button_onoff_indicator_on :
               	    android.R.drawable.button_onoff_indicator_off));
                mSnakeView.setMode(SnakeView.READY);
        	}
           return true;
        case R.id.menu_zoom:
        	if (mSnakeView.getMode() == SnakeView.PAUSE)
        	    showDialog(DIALOG_SIZE_ID);
        	else {
        	    mSnakeView.zoomTileSize();
                mSnakeView.setMode(SnakeView.READY);
        	}
            return true;
        case R.id.menu_speed:
        	if (mSnakeView.getMode() == SnakeView.PAUSE)
        	    showDialog(DIALOG_SPEED_ID);
        	else {
        	    mSnakeView.changeSpeed();
        	    item.setIcon(getResources().getDrawable(
               	    mSnakeView.mFast ? android.R.drawable.button_onoff_indicator_on :
               	    android.R.drawable.button_onoff_indicator_off));
                mSnakeView.setMode(SnakeView.READY);
        	}
           return true;
        case R.id.menu_tlrinput:
        	mSnakeView.tlrInputMode = !mSnakeView.tlrInputMode;
        	item.setIcon(getResources().getDrawable(
               	mSnakeView.tlrInputMode ? android.R.drawable.button_onoff_indicator_on :
               	android.R.drawable.button_onoff_indicator_off));
            setCorrectButtons();
        	return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog;
        Resources res = getBaseContext().getResources();
        AlertDialog.Builder builder;
        
        switch(id) {
        case DIALOG_ABOUT_ID:
        	dialog = new Dialog(this);
        	dialog.setContentView(R.layout.about_layout);
        	dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        	dialog.setTitle(res.getString(R.string.about_title));

            Button aboutDialogOkButton = (Button) dialog.findViewById(R.id.about_ok_button);
                aboutDialogOkButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(DIALOG_ABOUT_ID);
                    }
                });
            Button aboutDialogNewsButton = (Button) dialog.findViewById(R.id.about_news_button);
                aboutDialogNewsButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                    	showDialog(DIALOG_NEWS_ID);
                        dismissDialog(DIALOG_ABOUT_ID);
                    }
                });
            break;
        case DIALOG_NEWS_ID:
        	dialog = new Dialog(this);
        	dialog.setContentView(R.layout.news_layout);
        	dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
        	dialog.setTitle(res.getString(R.string.news_title));

            Button newsDialogOkButton = (Button) dialog.findViewById(R.id.news_ok_button);
                newsDialogOkButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                    	mSnakeView.showNews = false;
                        dismissDialog(DIALOG_NEWS_ID);
                    }
                });
            break;
        case DIALOG_RECORDS_ID:
        	dialog = new Dialog(this);
        	dialog.setContentView(R.layout.records_layout);
        	dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        	dialog.setTitle(res.getString(R.string.records_title));

            Button recordsDialogOkButton = (Button) dialog.findViewById(R.id.records_ok_button);
                recordsDialogOkButton.setOnClickListener(new Button.OnClickListener() {
                    public void onClick(View v) {
                        dismissDialog(DIALOG_RECORDS_ID);
                    }
                });
            break;
        case DIALOG_WALLS_ID:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage(res.getString(R.string.dlg_walls_title))
        	       .setCancelable(false)
//        	       .setIcon(R.drawable.headtile)
        	       .setPositiveButton(res.getString(R.string.dlg_yes),
        	    	   new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	           	   mSnakeView.changeWalls();
           	               mSnakeView.setMode(SnakeView.READY);
        	               dialog.cancel();
        	           }
        	       })
        	       .setNegativeButton(res.getString(R.string.dlg_no),
        	    	   new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
       	                   dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        case DIALOG_SIZE_ID:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage(res.getString(R.string.dlg_size_title))
        	       .setCancelable(false)
        	       .setPositiveButton(res.getString(R.string.dlg_yes),
        	    	   new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	           	   mSnakeView.zoomTileSize();
           	               mSnakeView.setMode(SnakeView.READY);
        	               dialog.cancel();
        	           }
        	       })
        	       .setNegativeButton(res.getString(R.string.dlg_no),
        	    	   new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
       	                   dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        case DIALOG_SPEED_ID:
        	builder = new AlertDialog.Builder(this);
        	builder.setMessage(res.getString(R.string.dlg_speed_title))
        	       .setCancelable(false)
        	       .setPositiveButton(res.getString(R.string.dlg_yes),
        	    	   new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
        	           	   mSnakeView.changeSpeed();
           	               mSnakeView.setMode(SnakeView.READY);
        	               dialog.cancel();
        	           }
        	       })
        	       .setNegativeButton(res.getString(R.string.dlg_no),
        	    	   new DialogInterface.OnClickListener() {
        	           public void onClick(DialogInterface dialog, int id) {
       	                   dialog.cancel();
        	           }
        	       });
        	dialog = builder.create();
        	break;
        default:
            dialog = null;
        }
        return dialog;
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog) {
        switch(id) {
        case DIALOG_RECORDS_ID:
            TextView mTView = (TextView) dialog.findViewById(R.id.records_layout_text);
            mTView.setText(mSnakeView.getRecordsText());
            break;
        }
    }

}
